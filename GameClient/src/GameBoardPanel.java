import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JPanel;


public class GameBoardPanel extends JPanel {

    private Gunner Gunner;
    private int width=1080;
    private int height=640;
    private static ArrayList<Obstacle> obstacles;
    private static ArrayList<Tree> trees;
    private static ArrayList<Gunner> Gunners;
    private boolean gameStatus;

    public GameBoardPanel(Gunner Gunner,Client client, boolean gameStatus)
    {
        this.Gunner=Gunner;
        this.gameStatus=gameStatus;
        setSize(width,height);
        setBounds(180,20,width,height);
        addKeyListener(new InputManager(Gunner));
        setFocusable(true);
        
        Gunners=new ArrayList<Gunner>(100);

        obstacles = new ArrayList<Obstacle>(50);
        obstacles.add(new Obstacle(220, 100));
        obstacles.add(new Obstacle(280, 100));
        obstacles.add(new Obstacle(220, 160));
        obstacles.add(new Obstacle(280, 160));
        //obstacles.add(new Obstacle(160, 420));
        obstacles.add(new Obstacle(220, 420));
        obstacles.add(new Obstacle(280, 420));
        obstacles.add(new Obstacle(280, 480));


        obstacles.add(new Obstacle(740, 100));
        obstacles.add(new Obstacle(740, 160));
        obstacles.add(new Obstacle(800, 160));
        obstacles.add(new Obstacle(740, 420));
        obstacles.add(new Obstacle(800, 420));
        obstacles.add(new Obstacle(740, 480));
        obstacles.add(new Obstacle(800, 480));

        //obstacles.add(new Obstacle(480, 260));
        //obstacles.add(new Obstacle(480, 320));
        //obstacles.add(new Obstacle(540, 260));
        //obstacles.add(new Obstacle(540, 320));

        trees = new ArrayList<Tree>(50);
        //trees.add(new Tree(1000,500));
        //trees.add(new Tree(1000, 560));
        //trees.add(new Tree(0,560));
        //trees.add(new Tree(0,500));

        for(int i=0;i<100;i++)
        {
            Gunners.add(null);
        }
   
    }
    public void paintComponent(Graphics gr) {
        super.paintComponent(gr);
        Graphics2D g = (Graphics2D) gr;
        g.setColor(Color.BLACK);
        g.fillRect(0,0, getWidth(),getHeight());
        g.drawImage(new ImageIcon("Images/bg.jpg").getImage(),50,50,this);

        if(gameStatus) 
        {
            Gunner.Draw(g, this);

            for(int i=1;i<Gunners.size();i++) 
            {
                if(Gunners.get(i)!=null)
                    Gunners.get(i).Draw(g, this);
            }

            for(int i=0; i<obstacles.size();i++)
            {
                if(obstacles.get(i)!=null)
                    obstacles.get(i).Draw(g, this);
            }

            for(int i=0; i<trees.size();i++)
            {
                if(trees.get(i)!=null)
                    trees.get(i).Draw(g, this);
            }

        }
        
        repaint();
    }

    public void registerNewGunner(Gunner newGunner)
    {
        Gunners.set(newGunner.getGunnerID(),newGunner);
    }
    public void removeGunner(int GunnerID) {
        Gunners.set(GunnerID, null);
    }

    public Gunner getGunner(int id)
    {
        return Gunners.get(id);
    }
    public void setGameStatus(boolean status)
    {
        gameStatus=status;
    }

    public boolean CheckWin()
    {
        for(int i=1; i<Gunners.size(); i++)
        {
            if(Gunners.get(i) != null)
                return false;
        }
        return true;
    }
  
    public static ArrayList<Gunner> getClients()
    {
        return Gunners;
    }
    public static ArrayList<Obstacle> getObstacles() { return obstacles; }
}
