import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class InputManager implements KeyListener  
{
    private final int LEFT = 37;
    private  final int RIGHT = 39;
    private final int UP = 38;
    private final int DOWN = 40;
    private static int status=0;    
    
    private Gunner Gunner;
    private Client client;

    public InputManager(Gunner Gunner) 
    {
        this.client=Client.getGameClient();
        this.Gunner=Gunner;
        
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) 
    {

        if(e.getKeyCode()==LEFT)
        {
            if(Gunner.getDirection()==1|Gunner.getDirection()==3)
            {
                
                Gunner.moveLeft();
                
                client.sendToServer(new Protocol().UpdatePacket(Gunner.getXposition(),
                          Gunner.getYposition(),Gunner.getGunnerID(),Gunner.getDirection()));
                
 
            }
            else if(Gunner.getDirection()==4)
            {
                Gunner.moveLeft();          
                client.sendToServer(new Protocol().UpdatePacket(Gunner.getXposition(),
                            Gunner.getYposition(),Gunner.getGunnerID(),Gunner.getDirection()));
            }
        }
        else if(e.getKeyCode()==RIGHT)
        {
            if(Gunner.getDirection()==1|Gunner.getDirection()==3)
            {
                Gunner.moveRight();                        
                client.sendToServer(new Protocol().UpdatePacket(Gunner.getXposition(),
                           Gunner.getYposition(),Gunner.getGunnerID(),Gunner.getDirection()));
                    
            }
            else if(Gunner.getDirection()==2)
            {
                Gunner.moveRight();
                client.sendToServer(new Protocol().UpdatePacket(Gunner.getXposition(),
                             Gunner.getYposition(),Gunner.getGunnerID(),Gunner.getDirection()));
            }
        }
        else if(e.getKeyCode()==UP)
        {
            if(Gunner.getDirection()==2|Gunner.getDirection()==4)
            {
                Gunner.moveForward();                            
                client.sendToServer(new Protocol().UpdatePacket(Gunner.getXposition(),
                          Gunner.getYposition(),Gunner.getGunnerID(),Gunner.getDirection()));
                        
            }
            else if(Gunner.getDirection()==1)
            {
                Gunner.moveForward();
                client.sendToServer(new Protocol().UpdatePacket(Gunner.getXposition(),
                        Gunner.getYposition(),Gunner.getGunnerID(),Gunner.getDirection()));
                            
            }
        }
        else if(e.getKeyCode()==DOWN)
        {
            if(Gunner.getDirection()==2|Gunner.getDirection()==4)
            {
                Gunner.moveBackward();
               
                client.sendToServer(new Protocol().UpdatePacket(Gunner.getXposition(),
                        Gunner.getYposition(),Gunner.getGunnerID(),Gunner.getDirection()));
                            
            }
            else if(Gunner.getDirection()==3)
            {
                Gunner.moveBackward();
                                    
                client.sendToServer(new Protocol().UpdatePacket(Gunner.getXposition(),
                                Gunner.getYposition(),Gunner.getGunnerID(),Gunner.getDirection()));
                                
            }
        }
        else if(e.getKeyCode()==KeyEvent.VK_SPACE)
        {
            client.sendToServer(new Protocol().ShotPacket(Gunner.getGunnerID()));
            Gunner.shot();
        }
    }

    public void keyReleased(KeyEvent e) {
    }
    
}
